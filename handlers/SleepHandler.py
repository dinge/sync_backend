from abc import ABCMeta, abstractmethod
from threading import Thread
from time import sleep


class SleepHandler(Thread, metaclass=ABCMeta):

    interval = None

    def __init__(self, interval):
        super().__init__()
        self.interval = interval

    def run(self):
        while True:
            self.func()
            sleep(self.interval)

    @abstractmethod
    def func(self):
        pass
