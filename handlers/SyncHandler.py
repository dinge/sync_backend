from handlers.SleepHandler import SleepHandler


class SyncHandler(SleepHandler):

    client_model = None

    def __init__(self, interval, client_model):
        super().__init__(interval)
        self.client_model = client_model

    def func(self):
        self.client_model.broadcast()
