from queue import Queue, Empty
from threading import Thread

import json


class LatencyDog(Thread):
    """
    waits 10 seconds for an end_time in the queue
    """
    q = None
    ring_buff = None
    start_time = None
    end_time = None

    def __init__(self, ring_buff, start_time):
        """
        init
        :param collections.deque.deque ring_buff: ring_buffer
        :param float start_time: start_time
        """
        super().__init__()
        self.q = Queue()
        self.ring_buff = ring_buff
        self.start_time = start_time
        self.start()

    def finish(self, end_time):
        """
        finishes dogging
        :param float end_time: end_time
        :return: None
        :rtype: None
        """
        self.q.put(end_time)

    def run(self):
        """
        thread run function
        :return: None
        :rtype: None
        """
        try:
            self.end_time = self.q.get(block=True, timeout=10)
        except Empty:
            print(json.dumps({"error": True, "message": "ping timed out"}))
        else:
            self._write_to_buffer()

    def _write_to_buffer(self):
        """
        writes start and end time to the ringbuffer
        :return: None
        :rtype: None
        """
        self.ring_buff.append({self.start_time: self.end_time})
