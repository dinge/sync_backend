import json
import time
from _sha512 import sha512
from collections import deque

import math
from autobahn.twisted import WebSocketServerProtocol
from os import urandom

from dependencies import get__client_model
from models.LatencyDog import LatencyDog


class Protocol(WebSocketServerProtocol):

    client_model = None
    latency_one = None
    latency_two = None
    latency = None
    dogs = None
    ring_buffer = None

    def __init__(self):
        super().__init__()
        self.client_model = get__client_model()
        self.ring_buffer = deque(maxlen=42)
        self.dogs = dict()

    def onMessage(self, payload, isBinary):
        payload = payload.decode("UTF-8")
        self.dispatch(payload)

    def dispatch(self, payload):
        head = payload[0:1]
        payload = payload[1:]
        if head == "p":
            self.recv_ping(payload)
        else:
            print("unknown method")

    def onOpen(self):
        print(json.dumps({"error": False, "message": "web socket connection opened"}))
        self.client_model.add_client(self)
        self.sendMessage("p".encode("UTF-8"))

    def onClose(self, wasClean, code, reason):
        print(json.dumps({"error": False, "message": "web socket connection closed"}))
        self.client_model.rem_client(self)

    @staticmethod
    def _get_weight(now, send_time, constant=0.5):
        return math.pow(1/(now-send_time), constant)

    def get_cleaned_latency(self):
        now = time.time()
        cnt = 0
        val = 0
        if len(self.ring_buffer) is not 0:
            for ping in self.ring_buffer:
                send = list(ping.keys())[0]
                recv = ping[send]
                weight = self._get_weight(now, send)
                cnt += weight
                val += weight * ((recv-send)/2)
            return val/cnt
        else:
            return 0

    def send_ping(self):
        identity = str(sha512(urandom(25)).hexdigest())
        self.dogs[identity] = LatencyDog(self.ring_buffer, time.time())
        self.sendMessage(str("p" + identity).encode("UTF-8"))

    def recv_ping(self, payload):
        latency_dog = self.dogs.get(payload)
        if latency_dog is None:
            print(json.dumps({"error": True, "message": "unknown_latency_dog"}))
        else:
            latency_dog.finish(time.time())
            del self.dogs[payload]
