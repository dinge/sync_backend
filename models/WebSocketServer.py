from threading import Thread

from autobahn.twisted import WebSocketServerFactory
from twisted.internet import reactor

from models.Protocol import Protocol


class WebSocketServer(Thread):
    def run(self):
        factory = WebSocketServerFactory()
        factory.protocol = Protocol

        reactor.listenTCP(10000, factory)
        reactor.run(installSignalHandlers=0)

