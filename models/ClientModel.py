from time import sleep, time


class ClientModel(object):
    clients = None

    def __init__(self):
        self.clients = list()

    def add_client(self, client):
        if client not in self.clients:
            self.clients.append(client)

    def rem_client(self, client):
        if client in self.clients:
            self.clients.remove(client)

    def get_clients(self):
        return self.clients

    def ping_all_clients(self):
        #print("pinging all")
        for client in self.clients:
            client.send_ping()

    def broadcast(self):
        clients = list(sorted(self.clients, key=lambda c: c.get_cleaned_latency()))
        if len(clients) is not 0:
            latencies = list(map(lambda c: c.get_cleaned_latency(), clients))
            latencies = list(map(lambda l: l-latencies[0], latencies))
            t = time()
            for i in list(range(len(latencies))):
                if not i == 0:
                    sleep_time = latencies[i] - latencies[i-1]
                    if sleep_time > 0:
                        sleep(sleep_time)
                clients[len(clients) -i -1].sendMessage(str("s"+str(t % 10)).encode("UTF-8"))
