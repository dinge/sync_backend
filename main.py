import json

from dependencies import get__client_model
from handlers.PingHandler import PingHandler
from handlers.SyncHandler import SyncHandler
from models.WebSocketServer import WebSocketServer

if __name__ == '__main__':
    client_model = get__client_model()

    wss = WebSocketServer()
    ping_handler = PingHandler(0.1, client_model)
    sync_handler = SyncHandler(1, client_model)

    wss.start()
    ping_handler.start()
    sync_handler.start()

    while 1:
        i = input()
        try:
            i = json.loads(i)
        except json.JSONDecodeError:
            print(json.dumps({"error": True, "message": "no valid json"}))
        else:
            print(i)
